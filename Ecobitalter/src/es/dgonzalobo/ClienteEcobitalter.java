/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 8
 * Se pide desarrollar un servidor, a partide un cliente dado, "ClienteEcobitalter", que bajo un protocolo descrito en el enunciado
 * se comunique por eco y con bit alternante con el mismo.
 * 
 */

package es.dgonzalobo;

import java.io.*;
import java.net.*;

public class ClienteEcobitalter {
	public static byte Idle=0, WaitAck=1;

	public static void main (String args[]) {
		boolean salir=false;
		String host = "localhost";
		int numlin=0, remotePort=7777; //puerto inicial del servidor 
		byte state=0;

		if (args.length > 0) { 
			host=args[0]; 
		} 
		try {
			InetAddress a = InetAddress.getByName(host); 
			DatagramSocket theSocket =new DatagramSocket(); 
			DatagramPacket dpout=null;
			BufferedReader lin=new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("\t****************************************************");
			System.out.println("\t                   ClienteEcobitalter");
			System.out.println("\t                   (PUERTO:"+remotePort+")");
			System.out.println("\t****************************************************\n");
			
			while (!salir){
				if(state==Idle){
					System.out.println("\nIntroudzca linea (acabar con '.'):");
					String line=lin.readLine();
					byte[] dataenv={(byte)(numlin%2)};
					line=(new String (dataenv))+line;
					dataenv=line.getBytes();
					dpout=new DatagramPacket(dataenv, dataenv.length,a,remotePort);
					theSocket.send(dpout); //envia un datagrama al servidor
					state=WaitAck; //cambia de estado para esperar asentimiento
				}
				else{
					try{
						theSocket.setSoTimeout(2000); //genera una excpetion tras 2 segundos.
													  //de espera por un datagrama de asentimiento
						DatagramPacket dpin=new DatagramPacket(new byte[256], 256);
						theSocket.receive(dpin);
						remotePort=dpin.getPort(); //puerto de la hebra del servidor que atiende a este cliente
						dpout.setPort(remotePort); 
						byte[] datarec=dpin.getData();
						if(datarec[0]==(byte)((numlin+1)%2)){ //Ack esperado
							System.out.println("Recibido ACK esprado: "+datarec[0]);
							String s=new String(datarec, 1,dpin.getLength()-1);
							System.out.println("ECO "+numlin+" de "+dpin.getAddress()+" : "+s);
							if(s.equals("."))
								salir=true; //recibe el eco de la linea final
							numlin++;
							state=Idle;
						}
						else{ //Ack no esperado 
							System.out.println("Recibido ACK NO esperado: "+datarec[0]+" - retranmision");
							theSocket.send(dpout);	
						}
					}
					catch(SocketTimeoutException e){
						System.out.println("timeout - retransmision");
						theSocket.send(dpout);
					}
				}
			}//while
			System.out.println("\nSeseion Finalizada.");
		}
		catch (UnknownHostException e) {
			System.out.println(e); 
		} 
		catch (IOException e) {} 
	} 
}

