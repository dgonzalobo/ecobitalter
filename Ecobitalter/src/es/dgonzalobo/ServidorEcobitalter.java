/**
 * @author dgonzalobo
 * (Desarrollado en UNIX)
 * 
 * PRACTICA 8
 * Se pide desarrollar un servidor, a partide un cliente dado, "ClienteEcobitalter", que bajo un protocolo descrito en el enunciado
 * se comunique por eco y con bit alternante con el mismo.
 * 
 */
package es.dgonzalobo;

import java.net.*;

public class ServidorEcobitalter {
	private static int PuertoServidor;
	private static DatagramSocket MiSocket; 
	private static ServidorEcobitalter server;
	private int i=0;

	/**
	 * @funcion incializa el puerto de escucha en el 1007, y arranca el socket por el que se esperar� a recibir UDPs del 
	 * 			cliente en dicho puerto.
	 */
	public ServidorEcobitalter(){
		PuertoServidor = 7777;
		i=0;
		try {
			MiSocket = new DatagramSocket(PuertoServidor);
		} 
		catch(Exception e) { 
			System.err.println("TalkServidor error: " + e); 
		} 
	}

	/**
	 * @funcion incializa el objeto propio de la clase por el que se invocar� el metodo listen server, se comprueba si en el
	 * 			arranque del programa se han introducido argumentos, de ser as� se le asignan al PuertoServidor, y se cambia
	 * 			el puerto donde escucha el socket.
	 * 			Se muestra una cabecer� indicando que programa es y en que puerto escucha y se llama al m�todo listen.
	 * @param args PuertoServidor
	 */
	public static void main (String[] args){
		server=new ServidorEcobitalter();
		if(args.length>0){
			PuertoServidor=Integer.parseInt(args[0]);
			try {
				MiSocket = new DatagramSocket(PuertoServidor);
			} 
			catch (SocketException e) {
				System.err.println("ERROR en el arranque del socket de escucha. "+e);
			}
		}

		System.out.println("\t****************************************************");
		System.out.println("\t                 ServidorEcobitalter");
		System.out.println("\t                   (PUERTO: "+PuertoServidor+")");
		System.out.println("\t****************************************************\n");

		while(true){
			server.listen();
		}
	}

	/**
	 * @funcion se crea un DatagramPacket en el que se alamacenar� el UDP recibido, se pone el a esuchar en el socket creado
	 * 			en el constructor. Una vez recibido el mensaje se crea un hilo a trav�s del cual se gestionar� la petici
	 */
	public void listen(){
		try{
			DatagramPacket PaqueteEntrada = new DatagramPacket(new byte[256],256); 
			MiSocket.receive(PaqueteEntrada); 
			ServidorEcobitalterThread ServThread=new ServidorEcobitalterThread(PaqueteEntrada, "ThreadServ "+i++);
			ServThread.start();
		}
		catch(NullPointerException npe){
			System.err.println("Port "+PuertoServidor+" it's already use");
			System.exit(0);
		}
		catch(Exception e){
			System.err.println("ERROR en escuchar p.principal. "+e);
			MiSocket.close();
			System.exit(0);
		}
	}
}


class ServidorEcobitalterThread extends Thread{
	private int sendPort, ack, clientPort;
	private String name;
	private DatagramSocket threadSocket;
	private InetAddress addr;
	private boolean flag;
	private DatagramPacket dpRecive;

	/**
	 * @funcion se le asigna un nombre al hilo, a fin de facilitar la depuracion. Se crea un puerto aleatorio, comprendido entre
	 * 			el 2000 y el 3000, puertos libres en MacOS. Se pone a false el flag, que determinar� si el hilo ha de acabar o no, 
	 * 			una vez recibido un punto. Se inicia el ThreadSocket en el puerto anteriormente generado. Se alamcena el 
	 * 			datagramPacket pasado por el programa principal a trav�s del constructor.
	 * @param paqueteEntrada datagramPacket recibido por el puerto 7, o sea datragamPacker de inicio de conexion
	 * @param n nombre del hilo
	 */
	public ServidorEcobitalterThread(DatagramPacket paqueteEntrada, String n){
		super(n);
		name="Client"+paqueteEntrada.getAddress();
		sendPort=(int)Math.floor(Math.random()*(5002-4501+1)+4501);
		clientPort=paqueteEntrada.getPort();
		flag=false;
		addr=paqueteEntrada.getAddress();
		ack=0;
		try{
			dpRecive=paqueteEntrada;
			threadSocket=new DatagramSocket(sendPort);
		}
		catch(Exception e){
			System.err.println("ERROR en la construccion del threadSocket "+e);
		}
	}

	/**
	 * @fucnion Se indica con quien se ha establecido conexion, se invoca el m�todo send para contestar al cliente. Se inicia un 
	 * 			while(true) donde si el flag no es true se seguir� escuchando y contestando, en caso contrario, se ejecutar� un break
	 * 			que generar� una exception, que es trata en el catch, haciendo un return al programa principal y dando as� por
	 * 			cerrado el hilo. Tambi�n se indica en dicho catch por pantalla el fin de la sesi�n con el cliente. 
	 * 			Se comprueba dos veces el flag, porque antes del while true se invoca un send con el UDP recibido en la incializacion
	 * 			de la sesi�n y pasado en la invoaci�n del Thread a trav�s del constructor, y dicho mensaje puede ser perfectamente
	 * 			un punto.
	 */
	public void run(){
		try{
			System.out.println("Connection accepted from: "+name);
			System.out.println("\tRecieved from "+name+"||Port/"+clientPort+": "+
							new String(dpRecive.getData(),1,dpRecive.getData().length-1));
			send();
			while(true){
				if(flag){
					System.out.println("Disconnection from: "+name);
					break;
				}
				listen();
				send();
				if(flag){
					System.out.println("Disconnection from: "+name);
					break;
				}
			}
		}
		catch(Exception e){
			System.out.println("Disconnection from: "+name);
			threadSocket.close();
			return;
		}
	}

	/**
	 * @funcion Se limpia el DatagramPacket sobre el que se va a recibir, a fin de evitar conflictos o corrupci�n de bytes. 
	 * 			Se escucha en el puerto generado en el constructor, y por el que ha pasado a realizarse las comunicaci�n de la 
	 * 			sesi�n que se encuentre ejecutando el Thread. Se almacena en dpReceive el datagrama recibido y se muestra por 
	 * 			pantalla la petici�n.
	 */
	public void listen(){
		try{
			dpRecive= new DatagramPacket(new byte[256],256); 
			threadSocket.receive(dpRecive); 
			System.out.println("\tRecieved from "+name+"||Port/"+clientPort+": "+
					new String(dpRecive.getData(),1,dpRecive.getData().length-1));
		}
		catch(Exception e){
			System.out.println("ERROR en escuchar en Thread "+e);
		}

	}

	/**
	 * @funcion Se crea un DatagramPacket a enviar, con el mensaje correspondiente, la direcci�n del cliente y el puerto en 
	 * 			el que se produce la comuniaci�n. Se env�a a trav�s del DatagramSocket abierto en el constructor, y si comprueba
	 * 			si el mensaje env�ado es un punto, en caso de ser as�, se proceder� a activar el flag que da paso al break dentro
	 * 			del while(true) del run, dando lugar al fin del hilo, y por tanto el fin de la sesi�n.
	 */
	public void send(){
		try{
			byte[] dataenv={(byte)(ack%2)};
			dataenv=(new String (dataenv)+new String(dpRecive.getData(), 1,dpRecive.getLength()-1)).getBytes();
			DatagramPacket dpSend=new DatagramPacket(dataenv, dataenv.length,addr,clientPort);
			threadSocket.send(dpSend); 
			System.out.println("\tSend to "+name+"||Port/"+clientPort+": "+new String(dataenv)+" <ACK="+ack+">");
			if((new String(dataenv, 1,dpRecive.getLength()-1)).equals(".")){
				flag=true;
			}
			changeACK();
		}
		catch(Exception e){
			System.err.println("ERROR en el send del Thread. "+e);
		}
	}

	/**
	 * @funcion cambia la variable int, ACK de 0 a 1 o de 1 a 0 dependiendo del estado en el que se encuentre en el momento en que
	 * 		    dicho m�todo es invocado.
	 */
	public void changeACK(){
		if(ack==0)
			ack=1;
		else
			ack=0;
	}
}
